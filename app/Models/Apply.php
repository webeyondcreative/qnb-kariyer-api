<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apply extends Model
{
    use HasFactory;

    public function members(){
        return $this->hasMany(ApplyMembers::class, 'apply_id', 'id');
    }
}
