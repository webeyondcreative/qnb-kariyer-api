<table>
    <thead>
    <tr>
        <th><b>#</b></th>
        <th><b>Adı - Soyadı</b></th>
        <th><b>TC Kimlik Numarası</b></th>
        <th><b>E-posta adresi</b></th>
        <th><b>İkamet Yeri</b></th>
        <th><b>Telefon Numarası</b></th>
        <th><b>Universite</b></th>
        <th><b>Fakülte</b></th>
        <th><b>Bölüm</b></th>
        <th><b>Sınıf</b></th>
        <th><b>Derecesi</b></th>
        <th><b>CV</b></th>
        <th><b>Doğum Tarihi</b></th>
        <th><b>Soru</b></th>
        <th><b>KVKK Durumu</b></th>
        <th><b>Başvuru Tarihi</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($applies as $key => $apply)

            <tr>
                <td>
                    {{ $key + 1 }}
                </td>
                <td>
                    {{ $apply->name }}
                </td>
                <td>
                    {{ $apply->tc }}
                </td>
                <td>
                    {{ $apply->email }}
                </td>
                <td>
                    {{ $apply->place }}
                </td>
                <td>
                    {{ $apply->gsm }}
                </td>
                <td>
                    {{ $apply->university }}
                </td>
                <td>
                    {{ $apply->faculty }}
                </td>
                <td>
                    {{ $apply->department }}
                </td>
                <td>
                    {{ $apply->classroom }}
                </td>
                <td>
                    {{ $apply->gpa }}
                </td>
                <td>
                    {{ $apply->file != '' ? \Illuminate\Support\Facades\Storage::disk('public')->url('/'.$apply->form_name.'/'.$apply->file) : '' }}
                </td>
                <td>
                    {{ $apply->birthday }}
                </td>
                <td>
                    {{ $apply->question }}
                </td>
                <td>
                    Onayladı.
                </td>
                <td>
                    @php
                        $date = \Carbon\Carbon::parse($apply->created_at, 'UTC');
echo $date->isoFormat('MMMM Do YYYY, h:mm:ss a');
                    @endphp
                </td>
            </tr>
    @endforeach

    </tbody>
</table>
