<?php

namespace App\Http\Controllers;

use App\Exports\ApplyExport;
use App\Models\Apply;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    public function index(){
        return view('welcome');
    }
    public function excel($form_name){
        return Excel::download(new ApplyExport($form_name), ''.$form_name.'-applies.xlsx');
    }
}
