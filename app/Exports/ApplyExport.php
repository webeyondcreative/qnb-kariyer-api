<?php

namespace App\Exports;

use App\Models\Apply;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class ApplyExport implements FromView
{
    public function __construct(string $form_name)
    {
        $this->form_name = $form_name;
    }

    public function view(): View
    {
        return view('exports.applies', [
            'applies' => Apply::where('form_name', $this->form_name)->get()
        ]);
    }
}
