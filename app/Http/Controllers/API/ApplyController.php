<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Apply;
use App\Models\ApplyIdeatrophy;
use App\Models\ApplyIdeatrophyMembers;
use App\Models\ApplyMembers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use function PHPUnit\Framework\isNull;

class ApplyController extends Controller
{
    public function apply(Request $req){

        $uniqueEmail= Apply::where('form_name', $req->form_name)->where('email', $req->email)->first();
        $uniqueTC= Apply::where('form_name', $req->form_name)->where('tc', $req->tc)->first();

        if (!is_null($uniqueEmail)){
            return response()->json(array("exists" => "unique_email"));
        }
        if (!is_null($uniqueTC)){
            return response()->json(array("exists" => "unique_tc"));
        }

        $apply = new Apply();

        if ($req->hasFile('file')) {
            $file = $req->file('file');

            $fileName = Str::slug($req->input('name')).'-'.Carbon::now()->toDateString().'-'.uniqid().'.'.$file->getClientOriginalExtension();
            if (!Storage::disk('public')->exists($req->form_name))
            {
                Storage::disk('public')->makeDirectory($req->form_name);
            }
            $req->file('file')->storeAs('public/'.$req->form_name.'/',$fileName);

            $apply->file = $fileName;
        }

        $apply->name = $req->name;
        $apply->tc = $req->tc;
        $apply->place = $req->place;
        $apply->email = $req->email;
        $apply->gsm = $req->gsm;
        $apply->university = $req->university;
        $apply->faculty = $req->faculty;
        $apply->department = $req->department;
        $apply->classroom = $req->classroom;
        $apply->gpa = $req->gpa;
        $apply->kvkk = $req->kvkk;
        $apply->form_name = $req->form_name;
        $apply->birthday = $req->birthday;
        $apply->question = $req->question;
        $apply->save();

        return response()->json(array("exists" => "success"));

    }
}
